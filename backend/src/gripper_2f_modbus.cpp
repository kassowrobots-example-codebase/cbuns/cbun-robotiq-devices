/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2024, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
#include <robotiq_devices/gripper_2f_modbus.h>
#include <kr2_program_api/internals/api/console.h>
#include <kr2_io_api/api_v1/protocols/modbus/modbus_exception.h>
#include <kr2_program_api/api_v1/bundles/arg_provider_xml.h>

#include <string>
#include <stdexcept>
#include <boost/algorithm/clamp.hpp>

#include <boost/interprocess/sync/named_mutex.hpp>

using namespace kswx_robotiq_devices;

Gripper2F_Modbus::Gripper2F_Modbus(boost::shared_ptr<kr2_program_api::ProgramInterface> a_api,
                                       const boost::property_tree::ptree &a_xml_bundle_node,
                                       int a_gripper_range)
:   kr2_bundle_api::CustomDevice(a_api, a_xml_bundle_node),
    gripper_range_(a_gripper_range)
{
    REGISTER_RPC(&Gripper2F_Modbus::open, this, ARG_INT(0), ARG_INT(1), ARG_BOOL(2), ARG_LOAD_OPT(3));
    REGISTER_RPC(&Gripper2F_Modbus::close, this, ARG_INT(0), ARG_INT(1), ARG_BOOL(2), ARG_LOAD_OPT(3));
    REGISTER_RPC(&Gripper2F_Modbus::move, this, ARG_INT(0), ARG_INT(1), ARG_INT(2), ARG_BOOL(3), ARG_LOAD_OPT(3));
    REGISTER_RPC(&Gripper2F_Modbus::stop, this);
    
    toolload_ = api_->variables_->allocSystemLoad("toolload", kr2rc_api::Load::SysId::LOAD_TOOL);
    payload_ = api_->variables_->allocSystemLoad("payload", kr2rc_api::Load::SysId::LOAD_PAYLOAD);
    
    std::string str_instance_label = node_.get<std::string>("<xmlattr>.label", "");
    ipc_id_ = "kswx_robotiq_devices." + str_instance_label;
}

Gripper2F_Modbus::~Gripper2F_Modbus()
{

}

int Gripper2F_Modbus::onCreate()
{
    // create shared payload
    std::string str_payload_id = ipc_id_ + ".payload";
    shr_payload_ = api_->variables_->allocSharedLoad(str_payload_id, true);
    *shr_payload_ = kr2_program_api::Load();
 
    // initialize shared memory
    using namespace boost::interprocess;
    shared_memory_object::remove(ipc_id_.c_str());
    shared_memory_object shm_object(create_only, ipc_id_.c_str(), read_write);
    shm_object.truncate(sizeof(SharedGripperData));
    shm_region_ = mapped_region(shm_object, read_write);
    shr_gripper_data_ = new (shm_region_.get_address()) SharedGripperData();
    
    SUBSCRIBE(kr2_signal::HWReady, Gripper2F_Modbus::onHWReady);
    
    if (mounting_tree_) {
        mount(*mounting_tree_);
    }
    
    return 0;
}

int Gripper2F_Modbus::onDestroy()
{
    
    onDeactivate(); 

    if (thread_.joinable()) {
        thread_.join();
    }
    
    if (!boost::interprocess::shared_memory_object::remove(ipc_id_.c_str())) {
        return -1;
    }
    
    return 0;
}

int Gripper2F_Modbus::onBind()
{
    // bind shared payload
    std::string str_payload_id = ipc_id_ + ".payload";
    shr_payload_ = api_->variables_->allocSharedLoad(str_payload_id, false);
    
    // bind shared memory
    using namespace boost::interprocess;
    shared_memory_object shm_object(open_only, ipc_id_.c_str(), read_write);
    shm_object.truncate(sizeof(SharedGripperData));
    shm_region_ = mapped_region(shm_object, read_write);
    shr_gripper_data_ = static_cast<SharedGripperData *>(shm_region_.get_address());
    
    SUBSCRIBE(kr2_signal::ProgramTerminated, Gripper2F_Modbus::onProgramTerminated);
    
    if (!activation_tree_) {
        CLOG_ERR("[CBUN/Gripper2F*] Activation params are not available");
        return -1;
    }
    
    if (!processActivationParams(*activation_tree_)) {
        CLOG_ERR("[CBUN/Gripper2F*] Failed to process activation params");
        return -2;
    }

    // open device connection
    if (!openConnection()) {
        CLOG_ERR("[CBUN/Gripper2F*] Failed to open connection");
        return -3;
    }
    
    return 0;
}

int Gripper2F_Modbus::onUnbind()
{
    if (activation_tree_) {
        if (!closeConnection()) {
            CLOG_ERR("[CBUN/Gripper2F*] Failed to close connection");
            return -1;
        }
    }
    return 0;
}

void Gripper2F_Modbus::onHWReady(const kr2_signal::HWReady &a_event)
{
    if (activation_tree_) {
        kr2_program_api::CmdResult<> result = activate(*activation_tree_);
        switch (result.result_) {
            case kr2_program_api::CmdResult<>::EXCEPTION:
                PUBLISH_EXCEPTION(result.code_, result.message_)
                break;
            case kr2_program_api::CmdResult<>::ERROR:
                PUBLISH_ERROR(result.code_, result.message_)
                break;
        }
    }
}

void Gripper2F_Modbus::onProgramTerminated(const kr2_signal::ProgramTerminated &a_event)
{
    program_terminated_ = true;
}

CBUN_PCALL Gripper2F_Modbus::onActivate(const boost::property_tree::ptree &a_param_tree)
{
    
    // process activation params
    if (!processActivationParams(a_param_tree)) {
        CBUN_PCALL_RET_ERROR(-1, "Invalid activation params");
    }
    
    // enable power supply
    isPowerSupplyEnabled_ = enablePowerSupply();
    if (!isPowerSupplyEnabled_) {
        CBUN_PCALL_RET_ERROR(-2, "Unable to enable power supply");
    }
    
    // open device connection
    isConnectionOpen_ = openConnection();
    if (!isConnectionOpen_) {
        CBUN_PCALL_RET_ERROR(-3, "Unable to open connection");
    }
    
    // activate device
    isDeviceActivated_ = activateDevice();
    if (!isDeviceActivated_) {
        CBUN_PCALL_RET_ERROR(-4, "Unable to initialize");
    }
    
    startControlThread();
    
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL Gripper2F_Modbus::onDeactivate()
{    
    // stop control thread
    stopControlThread();
    
    bool success = true;
    
    if (isDeviceActivated_) {
        if (deactivateDevice()) {
            isDeviceActivated_ = false;
        } else {
            CLOG_ERR("[CBUN/Gripper2F*] Failed to deactivate device");
            success = false;
        }
    }
    
    // close serial port
    if (isConnectionOpen_) {
        if (closeConnection()) {
            isConnectionOpen_ = false;
        } else {
            CLOG_ERR("[CBUN/Gripper2F*] Failed to close connection");
            success = false;
        }
    }
    
    // disable power supply
    if (isPowerSupplyEnabled_) {
        if (disablePowerSupply()) {
            isPowerSupplyEnabled_ = false;
        } else {
            CLOG_ERR("[CBUN/Gripper2F*] Failed to disable power supply");
            success = false;
        }
    }
    
    if (success) {
        CBUN_PCALL_RET_OK;
    } else {
        CBUN_PCALL_RET_ERROR(-1, "Unspecified error");
    }
}

CBUN_PCALL Gripper2F_Modbus::onMount(const boost::property_tree::ptree &a_param_tree)
{
    if (!processMountingParams(a_param_tree)) {
        CBUN_PCALL_RET_ERROR(-1, "Invalid mounting params");
    }
    
    *toolload_ = mounting_params_.toolload_;
    
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL Gripper2F_Modbus::onUnmount()
{
    *toolload_ = kr2_program_api::Load(0.0, kr2_program_api::Position(0.0, 0.0, 0.0), kr2_program_api::Imx(0.0, 0.0, 0.0, 0.0, 0.0, 0.0));
    
    CBUN_PCALL_RET_OK;
}

boost::shared_ptr< kr2_io_api::SerialPort > Gripper2F_Modbus::getPortByStringId(std::string a_strid) const
{
    // NOTE possibly add reading the DUID from GPIO mappings
    if (a_strid.empty()) return NULL;
    
    std::size_t found;
    found = a_strid.find("ToolBoard/RS485"); if (found !=std::string::npos) { return api_->io_api_->serial_->getPort(0x00300E01);}
    found = a_strid.find("IOBoard/RS485"); if (found !=std::string::npos) { return api_->io_api_->serial_->getPort(0x00000E02);}
    found = a_strid.find("IOBoard/RS232"); if (found !=std::string::npos) { return api_->io_api_->serial_->getPort(0x00000E01);}
    
    return api_->io_api_->serial_->getPort(a_strid.c_str());
}

CBUN_PCALL Gripper2F_Modbus::open(int a_speed, int a_force, bool a_block, boost::optional<kr2_program_api::Load> a_load)
{
    move(gripper_range_, a_speed, a_force, a_block, a_load);
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL Gripper2F_Modbus::close(int a_speed, int a_force, bool a_block, boost::optional<kr2_program_api::Load> a_load)
{
    move(0, a_speed, a_force, a_block, a_load);
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL Gripper2F_Modbus::move(int a_position, int a_speed, int a_force, bool a_block, boost::optional<kr2_program_api::Load> a_load)
{
    GripperStatus status;
    if (!getStatus(status)) {
        CBUN_PCALL_RET_ERROR(-1, "Failed to read status");
    }

    a_position = boost::algorithm::clamp(a_position, 0, gripper_range_);
    a_speed = boost::algorithm::clamp(a_speed, 0, 100);
    a_force = boost::algorithm::clamp(a_force, 0, 100);
    
    uint16_t position = (gripper_range_ - a_position) * 255 / gripper_range_; // ie. 0 mm = 0xFF, MAX(gripper_range_) mm = 0x00
    uint16_t speed_force = ((uint16_t) (a_speed * 2.55)) << 8;
    speed_force += a_force * 2.55;
    
    try {
        const uint16_t request_data[3] = {0x0900, position, speed_force};
        boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(shr_com_control_->access_mutex_);
        modbus_->writeRegisters(activation_params_.slave_address_, 0x03E8, 0x0003, request_data);
    } catch (kr2_io_api::ModbusException& e) {
        CLOG_ERR("[CBUN/Gripper2F*] Failed to send move request: " << e.what());
        CBUN_PCALL_RET_ERROR(e.code(), e.what());
    }
    
    if (a_block) {
        kr2_program_api::Number isMotionCompl;
        
        isMotionComplete(isMotionCompl);
    
        while (isMotionCompl.l() == 0L) {
            usleep(100000);
            isMotionComplete(isMotionCompl);
        }
    }
    
       // set load
    if (a_load && a_load->valid()) {
        *shr_payload_ = *a_load;
    }
    
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL Gripper2F_Modbus::moveEx(const kr2_program_api::Number &a_position, const kr2_program_api::Number &a_speed, const kr2_program_api::Number &a_force, const kr2_program_api::Number &a_wait)
{
    return move((int) a_position.l(), (int) a_speed.l(), (int) a_force.l(), a_wait.l() != 0, kr2_program_api::Load());
}

CBUN_PCALL Gripper2F_Modbus::stop()
{
    try {
        const uint16_t activate_data[3] = {0x0100, 0x0000, 0x0000};
        boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(shr_com_control_->access_mutex_);
        modbus_->writeRegisters(activation_params_.slave_address_, 0x03E8, 0x0003, activate_data);
    } catch (kr2_io_api::ModbusException& e) {
        CLOG_ERR("[CBUN/Gripper2F*] Failed to send stop request: " << e.what());
        CBUN_PCALL_RET_ERROR(e.code(), e.what());
    }
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL Gripper2F_Modbus::isObjectDetected(kr2_program_api::Number &a_target)
{
    GripperStatus status;
    if (!getStatus(status)) {
        CBUN_PCALL_RET_ERROR(-1, "Failed to read status");
    }

    switch (status.gOBJ) {
        case 1:
            a_target = (long) 1;
            break;
        case 2:
            a_target = (long) 2;
            break;
        default:
            a_target = (long) 0;
            break;
    }
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL Gripper2F_Modbus::isMotionComplete(kr2_program_api::Number &a_target)
{
    GripperStatus status;
    if (!getStatus(status, true)) {
        CBUN_PCALL_RET_ERROR(-1, "Failed to read status");
    }
    
    if (status.gGTO == 0 || status.gOBJ == 1 || status.gOBJ == 2 || status.gOBJ == 3){
         a_target = (long) 1;
    } else {
        a_target = (long) 0;
    }

    CBUN_PCALL_RET_OK;
}

CBUN_PCALL Gripper2F_Modbus::getPosition(kr2_program_api::Number &a_target)
{
    GripperStatus status;
    if (!getStatus(status)) {
        CBUN_PCALL_RET_ERROR(-1, "Failed to read status");
    }

    a_target = (long) ((255 - status.gPO) / 3);
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL Gripper2F_Modbus::isActivated(kr2_program_api::Number &a_target)
{
    GripperStatus status;
    if (!getStatus(status)) {
        CBUN_PCALL_RET_ERROR(-1, "Failed to read status");
    }

    if (status.gACT && status.gSTA == 3) {
        a_target = (long) 1;
    } else {
        a_target = (long) 0;
    }
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL Gripper2F_Modbus::eClose()
{
    try {
        const uint16_t activate_data[3] = {0x3100, 0x0000, 0x0000};
        boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(shr_com_control_->access_mutex_);
        modbus_->writeRegisters(activation_params_.slave_address_, 0x03E8, 0x0003, activate_data);
    } catch (kr2_io_api::ModbusException& e) {
        CLOG_ERR("[CBUN/Gripper2F*] Failed to send eClose request: " << e.what());
        CBUN_PCALL_RET_ERROR(e.code(), e.what());
    }
    
    usleep(10000);
    GripperStatus status;
    if (!getStatus(status)) {
        CBUN_PCALL_RET_ERROR(-1, "Failed to read status");
    }
    while (status.gFLT == 0x0B) {
        usleep(100000);
        if (!getStatus(status)) {
            CBUN_PCALL_RET_ERROR(-1, "Failed to read status");
        }
    }
    
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL Gripper2F_Modbus::eOpen()
{
    try {
        const uint16_t activate_data[3] = {0x1100, 0x0000, 0x0000};
        boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(shr_com_control_->access_mutex_);
        modbus_->writeRegisters(activation_params_.slave_address_, 0x03E8, 0x0003, activate_data);
    } catch (kr2_io_api::ModbusException& e) {
        CLOG_ERR("[CBUN/Gripper2F*] Failed to send eOpen request: " << e.what());
        CBUN_PCALL_RET_ERROR(e.code(), e.what());
    }
    
    usleep(10000);
    GripperStatus status;
    if (!getStatus(status)) {
        CBUN_PCALL_RET_ERROR(-1, "Failed to read status");
    }
    while (status.gFLT == 0x0B) {
        usleep(100000);
        if (!getStatus(status)) {
            CBUN_PCALL_RET_ERROR(-1, "Failed to read status");
        }
    }
        
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL Gripper2F_Modbus::activateDevice(bool a_wait)
{
    // activate device
    if (!activateDevice()) {
        CBUN_PCALL_RET_ERROR(-4, "Unable to initialize");
    }
    
    CBUN_PCALL_RET_OK;
}

#define OBJ_DETECTED(__status) (__status.gOBJ == 1 || __status.gOBJ == 2)

void Gripper2F_Modbus::startControlThread()
{
    if (thread_.joinable()) {
        thread_.join();
    }
    running_.store(true, std::memory_order_release);
    thread_ = std::thread([this]() {
        
        while (running_.load(std::memory_order_acquire)) {
            
            if (!readStatus()) {
                PUBLISH_EXCEPTION(-1, "Failed to read device status")
                onDeactivate(); // Lightweight version of deactivate
                node_.erase("activation"); // To let the GUI know, that the device is deactivated
                continue;
            }
            
            if (!OBJ_DETECTED(status_tm1_) && OBJ_DETECTED(shr_gripper_data_->status_)) {
                if (shr_payload_->valid()) {
                    payload_ = shr_payload_;
                }
            } else if (OBJ_DETECTED(status_tm1_) && !OBJ_DETECTED(shr_gripper_data_->status_)) {
                if (shr_payload_->valid()) {
                    *payload_ = kr2_program_api::Load(0.0, kr2_program_api::Position(0.0, 0.0, 0.0), kr2_program_api::Imx(0.001, 0.001, 0.001, 0.0, 0.0, 0.0));
                }
            }
            
            usleep(100000);
        }
    });
}

bool Gripper2F_Modbus::readStatus()
{
    uint16_t response[3];
    
    // read data from gripper (synchronized)
    try {
        boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(shr_com_control_->access_mutex_);
        modbus_->readRegisters(activation_params_.slave_address_, 0x07D0, 0x0003, response);
    } catch (kr2_io_api::ModbusException& e) {
        return false;
    }
    
    uint8_t gripper_status = response[0] >> 8;
    uint8_t fault_status = response[1] >> 8;
    
    shr_gripper_data_->status_mutex_.lock();
    status_tm1_ = shr_gripper_data_->status_;
    shr_gripper_data_->status_.gACT = gripper_status & 1;
    shr_gripper_data_->status_.gGTO = (gripper_status >> 3) & 1;
    shr_gripper_data_->status_.gSTA = (gripper_status >> 4) & 3;
    shr_gripper_data_->status_.gOBJ = (gripper_status >> 6) & 3;
    
    shr_gripper_data_->status_.gFLT = fault_status & 15;
    shr_gripper_data_->status_.kFLT = (fault_status >> 4);
    shr_gripper_data_->status_.gPO = response[2] >> 8;
    shr_gripper_data_->status_mutex_.unlock();
    
    return true;
}

void Gripper2F_Modbus::stopControlThread()
{
    running_.store(false, std::memory_order_release);
//     if (thread_.joinable()) {
//         thread_.join();
//     }
}

bool Gripper2F_Modbus::getStatus(Gripper2F_Modbus::GripperStatus &a_status, bool a_force_refresh)
{
    if (a_force_refresh) {
        if (!readStatus()) {
            return false;
        }
    }
    boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(shr_gripper_data_->status_mutex_);
    a_status = shr_gripper_data_->status_;
    return true;
}

bool Gripper2F_Modbus::processActivationParams(const boost::property_tree::ptree &a_param_tree)
{
    kr2_bundle_api::ArgProviderXml arg_provider(a_param_tree);
    if (arg_provider.getArgCount() != 3) {
        CLOG_ERR("[CBUN/Gripper2F*] Unexpected param count: actual=" << arg_provider.getArgCount() << ", expected=" << 3);
        return false;
    }
    
    activation_params_.device_path_ = arg_provider.getString(0);
    
    std::string str_slave_address = arg_provider.getString(1);
    try {
        activation_params_.slave_address_ = (unsigned char)std::stoul(str_slave_address, nullptr, 0);
    } catch (const std::logic_error &e) {
        CLOG_ERR("[CBUN/Gripper2F*] Invalid slave address=" << str_slave_address);
        return false;
    }
    
    activation_params_.power_supply_ = arg_provider.getInt(2);
    
    return true;
}

bool Gripper2F_Modbus::processMountingParams(const boost::property_tree::ptree &a_param_tree)
{
    kr2_bundle_api::ArgProviderXml arg_provider(a_param_tree);
    if (arg_provider.getArgCount() != 1) {
        CLOG_ERR("[CBUN/Gripper2F*] Unexpected param count: actual=" << arg_provider.getArgCount() << ", expected=" << 1);
        return false;
    }
    
    mounting_params_.toolload_ = arg_provider.getLoad(0);
    
    return true;
}

bool Gripper2F_Modbus::enablePowerSupply()
{
    // enable power supply (optional)
    switch (activation_params_.power_supply_) {
        case 3178769:
        case 3178770:
        case 3178771:
        case 3178753:
        case 3178754:
        case 3178755:
        case 3178756:
        {
            // ToolBoard Power Supply
            kr2rc_api::IOData::CmdTXGPIOParams cmd_params;
            kr2rc_api::IOData::GPIOInt64 digital { activation_params_.power_supply_, 1, 0x08010018 };
            kr2rc_api::CmdResult result = api_->rc_api_->iob_data_->cmd_TX_GPIO(cmd_params, NULL, 0, &digital, 1, NULL, 0);
            if (result.err_code_ != 0) {
                return false;
            }
            usleep(2500000);
            break;
        }
        case 33041:
        case 33042:
        case 33043:
        case 33044:
        {
            // IOBoard Digital Output
            kr2rc_api::IOData::CmdTXGPIOParams cmd_params;
            kr2rc_api::IOData::GPIOInt64 digital { activation_params_.power_supply_, 1, 0 };
            kr2rc_api::CmdResult result = api_->rc_api_->iob_data_->cmd_TX_GPIO(cmd_params, NULL, 0, &digital, 1, NULL, 0);
            if (result.err_code_ != 0) {
                return false;
            }
            usleep(2500000);
            break;
        }
        default:
            break;
    }
    
    return true;
}

bool Gripper2F_Modbus::disablePowerSupply()
{
    if (activation_params_.power_supply_) {
        kr2rc_api::IOData::CmdTXGPIOParams cmd_params;
        kr2rc_api::IOData::GPIOInt64 digital { activation_params_.power_supply_, 0, 0 };
        kr2rc_api::CmdResult result = api_->rc_api_->iob_data_->cmd_TX_GPIO(cmd_params, NULL, 0, &digital, 1, NULL, 0);
        if (result.err_code_ != 0) {
            return false;
        }
    }
    
    return true;
}

bool Gripper2F_Modbus::openConnection()
{
    // open serial port
    port_ = getPortByStringId(activation_params_.device_path_);
    port_->open();
    port_->configure(115200, 8, 1, 0);
    
    // initialize Modbus RTU
    modbus_ = kr2_io_api::ModbusSerialMaster::factory(kr2_io_api::ModbusSerialMaster::RTU);
    modbus_->setRetryCount(5);
    modbus_->init(port_);
    
    // init synchronization
    shr_com_memory_ = boost::interprocess::managed_shared_memory(boost::interprocess::open_or_create, "kswx_robotiq_devices", 4096);
    shr_com_control_ = shr_com_memory_.find_or_construct<SharedComControl>(activation_params_.device_path_.c_str())();
    shr_com_control_->acquire();
    
    return true;
}

bool Gripper2F_Modbus::closeConnection()
{
    // deinit synchronization
    if (shr_com_control_->release() == 0) {
        if (!shr_com_memory_.destroy<SharedComControl>(activation_params_.device_path_.c_str())) {
            CLOG_ERR("Failed to destruct shared com control \"" << activation_params_.device_path_ << "\": Shared memory could not be destroyed");
        }
        boost::interprocess::shared_memory_object::remove("kswx_robotiq_devices");
    }
    
    // close serial port
    try {
        if (port_ && port_->isOpen()){
            port_->close();
        }
    } catch (std::exception &e) {
        return false;
    }
    
    return true;
}

bool Gripper2F_Modbus::activateDevice()
{
    // provide device activation
    GripperStatus status;
    
    boost::interprocess::named_mutex activation_mutex(boost::interprocess::open_or_create, "kswx_robotiq_devices.mutex");
    boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock(activation_mutex);
    
    usleep(1000000);
    
    // clear rACT
    try {
        const uint16_t clear_data[3] = {0x0000, 0x0000, 0x0000};
        boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(shr_com_control_->access_mutex_);
        modbus_->writeRegisters(activation_params_.slave_address_, 0x03E8, 0x0003, clear_data);
        usleep(10000);
    } catch (kr2_io_api::ModbusException& e) {
        CLOG_ERR("Failed to clear rACT: " << e.what());
        return false;
    }
    
    // set rACT (activate)
    try {
        const uint16_t activate_data[3] = {0x0100, 0x0000, 0x0000};
        boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(shr_com_control_->access_mutex_);
        modbus_->writeRegisters(activation_params_.slave_address_, 0x03E8, 0x0003, activate_data);
        usleep(10000);
    } catch (kr2_io_api::ModbusException& e) {
        CLOG_ERR("Failed to set rACT: " << e.what());
        return false;
    }
    
    // wait for activation complete
    if (!getStatus(status, true)) {
        return false;
    }
    while (status.gACT == 1 && status.gSTA == 1) {
        usleep(100000);
        if (!getStatus(status, true)) {
            return false;
        }
    }
    return true;
}

bool Gripper2F_Modbus::deactivateDevice()
{
    // clear rACT
    try {
        const uint16_t clear_data[3] = {0x0000, 0x0000, 0x0000};
        boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(shr_com_control_->access_mutex_);
        modbus_->writeRegisters(activation_params_.slave_address_, 0x03E8, 0x0003, clear_data);
    } catch (std::exception &e) {
        CLOG_WARN("[CBUN/Gripper2F*] Failed to clear rACT: " << e.what());
        return false;
    }
    
    usleep(100000);
    
    return true;
}
