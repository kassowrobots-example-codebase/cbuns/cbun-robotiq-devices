/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2019, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/
#include <kr2_program_api/api_v1/bundles/custom_device.h>
#include <kr2_io_api/api_v1/protocols/modbus/master/serial_master.h>

namespace kswx_robotiq_devices {

    class Gripper2F_Modbus : public kr2_bundle_api::CustomDevice {
    public:
        
        Gripper2F_Modbus(boost::shared_ptr<kr2_program_api::ProgramInterface> api,
                           const boost::property_tree::ptree &xml_bundle_node,
                           int gripper_range);
        
        virtual ~Gripper2F_Modbus();
        
        // fixed bundle class methods
        
        virtual int onCreate();
        virtual int onDestroy();
        
        virtual int onBind();
        virtual int onUnbind();
        
        void onHWReady(const kr2_signal::HWReady&);
        void onProgramTerminated(const kr2_signal::ProgramTerminated&);

        
        // custom (used defined) methods
        
        virtual CBUN_PCALL open(int speed, int force, bool wait, boost::optional<kr2_program_api::Load> load);
        virtual CBUN_PCALL close(int speed, int force, bool wait, boost::optional<kr2_program_api::Load> load);
        virtual CBUN_PCALL move(int position, int speed, int force, bool wait, boost::optional<kr2_program_api::Load> load);
        virtual CBUN_PCALL stop();
        
        virtual CBUN_PCALL moveEx(const kr2_program_api::Number &position, const kr2_program_api::Number &speed, const kr2_program_api::Number &force, const kr2_program_api::Number &wait);
        
        virtual CBUN_PCALL isObjectDetected(kr2_program_api::Number &target);
        virtual CBUN_PCALL isMotionComplete(kr2_program_api::Number &target);
        virtual CBUN_PCALL getPosition(kr2_program_api::Number &target);
        
        virtual CBUN_PCALL isActivated(kr2_program_api::Number &target);
        virtual CBUN_PCALL activateDevice(bool wait);
        virtual CBUN_PCALL eClose();
        virtual CBUN_PCALL eOpen();
        
    protected:
        
        // fixed custom device methods
        
        virtual CBUN_PCALL onActivate(const boost::property_tree::ptree &param_tree);
        virtual CBUN_PCALL onDeactivate();
        
        virtual CBUN_PCALL onMount(const boost::property_tree::ptree &param_tree);
        virtual CBUN_PCALL onUnmount();
        
    private:
        
        struct {
            std::string device_path_ = "ToolBoard/RS485";
            unsigned char slave_address_ = 0x09;
            kr2rc_api::DUID power_supply_ = 0;
        } activation_params_;
        
        bool processActivationParams(const boost::property_tree::ptree &tree);
        
        struct {
            kr2_program_api::Load toolload_;
        } mounting_params_;
        
        bool processMountingParams(const boost::property_tree::ptree &tree);
        
        bool enablePowerSupply();
        bool disablePowerSupply();
        bool isPowerSupplyEnabled_ = false;
        
        bool openConnection();
        bool closeConnection();
        bool isConnectionOpen_ = false;
        
        bool activateDevice();
        bool deactivateDevice();
        bool isDeviceActivated_ = false;
        
        struct GripperStatus {
            bool gACT;
            bool gGTO;
            uint gSTA;
            uint gOBJ;
            uint gFLT;
            uint kFLT;
            uint gPO;
            GripperStatus() : gACT(false), gGTO(false), gSTA(0), gOBJ(0), gFLT(0), kFLT(0), gPO(0) {}
            ~GripperStatus() {}
        };
        
        struct SharedGripperData {
            boost::interprocess::interprocess_mutex comm_mutex_;
            boost::interprocess::interprocess_mutex status_mutex_;
            GripperStatus status_;
        };
        
        boost::shared_ptr<kr2_io_api::SerialPort> port_;
        boost::shared_ptr<kr2_io_api::ModbusSerialMaster> modbus_;
        const int gripper_range_;
        
        boost::shared_ptr<kr2_program_api::Load> toolload_;
        boost::shared_ptr<kr2_program_api::Load> shr_payload_;
        boost::shared_ptr<kr2_program_api::Load> payload_;
        
        // Shared Memory
        std::string ipc_id_;
        boost::interprocess::mapped_region shm_region_;
        
        // Read Status Loop
        std::atomic<bool> running_;
        std::thread thread_;
        void startControlThread();
        bool readStatus();
        void stopControlThread();
        bool getStatus(GripperStatus &status, bool force_refresh=false);
        
        
        std::atomic<bool> program_terminated_;
        
        GripperStatus status_tm1_;
        
        SharedGripperData *shr_gripper_data_;
    
        
        boost::shared_ptr<kr2_io_api::SerialPort> getPortByStringId(std::string a_strid) const;
        
        struct SharedComControl {
            boost::interprocess::interprocess_mutex access_mutex_;
            unsigned int instances_ = 0;
            unsigned int acquire() {
                using namespace boost::interprocess;
                scoped_lock<interprocess_mutex> lock(access_mutex_);
                instances_ = instances_ + 1;
                return instances_;
            }
            unsigned int release() {
                using namespace boost::interprocess;
                scoped_lock<interprocess_mutex> lock(access_mutex_);
                instances_ = instances_ - 1;
                return instances_;
            }
        };
        
        boost::interprocess::managed_shared_memory shr_com_memory_;
        SharedComControl *shr_com_control_ = 0;
    };
    
}
